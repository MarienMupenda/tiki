# lib/smarty_tiki/

This is where all the Tiki specific [Smarty](https://www.smarty.net/) modifiers, blocks and functions live.
